# CICD SAST DAST Tooling

![JavaScript](https://img.shields.io/badge/-JavaScript-F7DF1E?style=for-the-badge&logo=JavaScript&logoColor=black)
![Node.js](https://img.shields.io/badge/-Node.js-339933?style=for-the-badge&logo=node.js&logoColor=white)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![Terraform](https://img.shields.io/badge/terraform-7B42BC?logo=terraform&logoColor=white&style=for-the-badge)
![VSCode](https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white)
![kubernetes](https://img.shields.io/badge/kubernetes-326CE5?logo=kubernetes&logoColor=white&style=for-the-badge)
![Azure](https://img.shields.io/badge/azure-0078D4?logo=microsoft-azure&logoColor=white&style=for-the-badge)
![Amazon](https://img.shields.io/badge/Amazon_AWS-232F3E?style=for-the-badge&logo=amazon-aws&logoColor=white)
![Gcp](https://img.shields.io/badge/Google_Cloud-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white)
![Docker](https://img.shields.io/badge/docker-2496ED?logo=docker&logoColor=white&style=for-the-badge)
![python](https://img.shields.io/badge/python-3776AB?logo=python&logoColor=white&style=for-the-badge)
![golang](https://img.shields.io/badge/Go-00ADD8?style=for-the-badge&logo=go&logoColor=white)
![github Actions](https://img.shields.io/badge/GitHub_Actions-2088FF?style=for-the-badge&logo=github-actions&logoColor=white)
![Gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)
![Jenkins](	https://img.shields.io/badge/Jenkins-D24939?style=for-the-badge&logo=Jenkins&logoColor=white)
![city](https://img.shields.io/badge/TeamCity-000000?style=for-the-badge&logo=TeamCity&logoColor=white)

---
![](https://github.com/roxsross/roxsross/blob/main/images/roxsross-banner-1.png?raw=true)

🔥🔥🔥🔥

### by RoxsRoss

### CICD TOOLING

### Sobre DevSecOps
DevSecOps es una práctica emergente en el desarrollo de software que integra la seguridad (Sec) en el ciclo de vida del desarrollo de software (Dev) y las operaciones de TI (Ops). El objetivo principal de DevSecOps es asegurar que la seguridad se considere en cada fase del ciclo de vida del desarrollo, desde la planificación y el diseño hasta el desarrollo, las pruebas, el despliegue y las operaciones continuas.

### Porque usar SAST en Herramientas de CICD
El uso de SAST (Static Application Security Testing) en herramientas de CI/CD es crucial por varias razones relacionadas con la seguridad, la calidad del código y la eficiencia del desarrollo

**Herramientas de CI:**
1. **Jenkins**: Una de las herramientas de CI/CD más populares, es altamente extensible con numerosos plugins.
2. **Github Actions**: Una herramienta de CI basada en la nube que es especialmente popular en proyectos de código abierto.
3. **GitLab CI/CD**: Integrado con GitLab, ofrece una solución completa de CI/CD.
4. **Azure DevOps**: Ofrece servicios de CI/CD, gestión de repositorios y herramientas de colaboración.

### **Despliegue Continuo (CD)**
El Despliegue Continuo extiende la Integración Continua al automatizar el despliegue del código en entornos de producción, asegurando que cada cambio que pase las pruebas automatizadas pueda ser liberado de manera automática.

**Herramientas de CD:**
1. **Spinnaker**: Desarrollada por Netflix, es una herramienta de despliegue continuo multi-nube.
2. **Argo CD**: Una herramienta declarativa de despliegue continuo para Kubernetes.


### **Beneficios del Tooling de CI/CD**
1. **Automatización**: Minimiza la intervención manual, reduciendo errores humanos y mejorando la eficiencia.
2. **Retroalimentación Rápida**: Permite detectar y corregir errores rápidamente mediante pruebas automatizadas continuas.
3. **Entrega Continua**: Facilita la entrega continua de nuevas funcionalidades y correcciones de errores, acelerando el tiempo de comercialización.
4. **Consistencia y Calidad**: Asegura que el código sea probado y validado de manera consistente, mejorando la calidad general del software.
5. **Colaboración y Transparencia**: Mejora la colaboración entre los equipos de desarrollo, pruebas y operaciones, y proporciona visibilidad sobre el estado del proyecto.

### **Implementación de CI/CD**
La implementación de un pipeline de CI/CD generalmente implica los siguientes pasos:

1. **Configuración del Repositorio de Código**: Centralización del código fuente en un sistema de control de versiones (por ejemplo, Git).
2. **Automatización de Pruebas**: Creación de scripts de prueba automatizados que se ejecutan cada vez que se integra nuevo código.
3. **Build Automation**: Configuración de scripts para compilar el código y crear artefactos de construcción.
4. **Deployments Automatizados**: Definición de scripts y configuraciones para desplegar automáticamente el software en entornos de prueba, staging y producción.
5. **Monitoreo y Alertas**: Implementación de herramientas de monitoreo para vigilar el rendimiento y la estabilidad del software desplegado, con alertas para notificar a los equipos de cualquier problema.

### **Ejemplos de Pipelines de CI/CD**

- **Pipeline de Jenkins**: Un archivo Jenkinsfile define las etapas del pipeline, incluyendo build, test y deploy.
- **Pipeline de GitLab CI**: Usando un archivo `.gitlab-ci.yml` para definir las etapas y los jobs, ejecutados en runners configurados.


### **Repos Vulnerables**

- **Netflix Clone**: Este proyecto establece un entorno DevOps sólido para un desarrollo, integración e implementación de software fluidos
    - Codigo en repositorio [eko-netflix-clone](https://gitlab.com/roxsross/eko-insecure-web/-/tree/eko-netflix-clone?ref_type=heads)
- **OWASP Juice Shop**: ¡OWASP Juice Shop es probablemente la aplicación web insegura más moderna y sofisticada! ¡Se puede utilizar en capacitaciones de seguridad, demostraciones de concientización, CTF y como conejillo de indias para herramientas de seguridad!
    - Codigo Oficial [github](https://github.com/juice-shop/juice-shop)
    - Codigo en repositorio [eko-juice-shop](https://gitlab.com/roxsross/eko-insecure-web/-/tree/eko-juice-shop?ref_type=heads)

### **Instalables**

- **Defectdojo**:  es una plataforma de orquestación de seguridad y gestión de vulnerabilidades. Esta guía rápida te ayudará a comenzar con DefectDojo utilizando la demo pública.
    - Información [enlace](./defectdojo/Install-defectdojo.md)
    - Script python para subir reportes [upload-report](./defectdojo/upload-report.py)
- **Instalación Runner Gitlab**: Es una plataforma colaborativa de desarrollo de software para proyectos DevOps grandes. Proporciona capacidades DevOps de extremo a extremo para cada etapa del ciclo de vida de desarrollo de software.
    - Información de como instalar [linux](./gitlab/linux/install-runner-linux.md)
    - Información de como instalar [docker](./gitlab/docker/install-runner-linux.md)
- **Gitlab Pipeline basic**: Como construir un pipeline en gitlab
    - Arquitectura de pipeline basic [gitlab](./gitlab/pipeline-basic/.gitlab-ci.yml)
---
🔥🔥🔥🔥

<img width="80%" src="https://roxsross-linktree.s3.amazonaws.com/295-full-website-banner-transparent-black.png"> 


### ✉️  &nbsp;Contactos 

Me puedes encontrar en:

[![site](https://img.shields.io/badge/Hashnode-2962FF?style=for-the-badge&logo=hashnode&logoColor=white&link=https://blog.295devops.com) ](https://blog.295devops.com)
[![Blog](https://img.shields.io/badge/dev.to-0A0A0A?style=for-the-badge&logo=devdotto&logoColor=white&link=https://dev.to/roxsross)](https://dev.to/roxsross)
![Twitter](https://img.shields.io/twitter/follow/roxsross?style=for-the-badge)
[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=for-the-badge&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/roxsross/)](https://www.linkedin.com/in/roxsross/)
[![Instagram Badge](https://img.shields.io/badge/-Instagram-purple?style=for-the-badge&logo=instagram&logoColor=white&link=https://www.instagram.com/roxsross)](https://www.instagram.com/roxsross/)
[![Youtube Badge](https://img.shields.io/badge/YouTube-FF0000?style=for-the-badge&logo=youtube&logoColor=white&link=https://www.youtube.com/channel/UCa-FcaB75ZtqWd1YCWW6INQ)](https://www.youtube.com/channel/UCa-FcaB75ZtqWd1YCWW6INQ)


<samp>
"Para entender algo no debes entenderlo sino serlo"
<samp>
  </div>
  